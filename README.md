Write a software that will solve following problem using Java/Scala/Groovy:

There is a university with multiple courses. Each of them can be taken by any student, but they are limited to certain number of participants, number can vary between courses.
Your program will register students when they apply taking into account participant number constraint. It should work in concurrent environment (multiple students can apply four a course at the same time).
