CREATE DATABASE `OP_DB`;

USE `OP_DB`;

CREATE TABLE `course` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `student_limit` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `student` (
  `username` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `student_course` (
  `students_username` varchar(255) NOT NULL,
  `courses_id` bigint(20) NOT NULL,
  KEY (`courses_id`),
  KEY (`students_username`),
  FOREIGN KEY (`students_username`) REFERENCES `student` (`username`),
  FOREIGN KEY (`courses_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_role` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `student_user_role` (
  `Student_username` varchar(255) NOT NULL,
  `roles_id` bigint(20) NOT NULL,
  KEY (`roles_id`),
  KEY (`Student_username`),
  FOREIGN KEY (`roles_id`) REFERENCES `user_role` (`id`),
  FOREIGN KEY (`Student_username`) REFERENCES `student` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `OP_DB`.`user_role` (`id`, `name`) VALUES ('0', 'STUDENT');
INSERT INTO `OP_DB`.`hibernate_sequence` VALUES ('1');



