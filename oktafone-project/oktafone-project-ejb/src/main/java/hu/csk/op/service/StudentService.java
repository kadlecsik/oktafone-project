package hu.csk.op.service;

import hu.csk.op.dto.CourseDTO;
import hu.csk.op.dto.StudentDTO;
import hu.csk.op.entity.Course;
import hu.csk.op.entity.Student;
import hu.csk.op.exception.CourseIsFullException;
import hu.csk.op.facade.EntityFacade;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.hibernate.dialect.lock.OptimisticEntityLockException;

@Stateless
public class StudentService {

    @Inject
    private EntityFacade entityFacade;

    public List<CourseDTO> getCoursesForStudentByStudentId(String id) {
        Student student = entityFacade.find(Student.class, id);

        List<CourseDTO> ret = new LinkedList<>();
        student.getCourses().stream().forEach(c -> ret.add(new CourseDTO(c)));

        return ret;
    }

    public StudentDTO getStudentById(String id) {
        return new StudentDTO(entityFacade.find(Student.class, id));
    }

    public void subscribeToCourseByIds(String studentId, Long courseId) {
        Student student = entityFacade.find(Student.class, studentId);
        Course course = entityFacade.find(Course.class, courseId);

        if (!student.getCourses().contains(course)) {
            boolean re;
            do {
                if (course.getStudents().size() >= course.getLimit()) {
                    throw new CourseIsFullException();
                }
                re = false;
                try {
                    course.getStudents().add(student);
                    student.getCourses().add(course);
                    entityFacade.update(course);
                    entityFacade.update(student);
                } catch (OptimisticEntityLockException ex) {
                    student.getCourses().remove(course);
                    course = entityFacade.find(Course.class, courseId);
                    re = true;
                }
            } while (re);

        }

    }

    public void unSubscribeFromCourseByIds(String studentId, Long courseId) {
        Student student = entityFacade.find(Student.class, studentId);
        Course course = entityFacade.find(Course.class, courseId);

        course.getStudents().remove(student);
        student.getCourses().remove(course);

        entityFacade.update(course);
        entityFacade.update(student);
    }

}
