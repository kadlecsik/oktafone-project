package hu.csk.op.facade;

import hu.csk.op.entity.Role;
import hu.csk.op.entity.Student;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Stateless
public class UserFacade {

    @PersistenceContext(unitName = "OP_PU")
    private EntityManager entityManager;

    public Student getStudentByUsername(String username) {
        try {
            return entityManager.createQuery("SELECT s FROM Student s WHERE s.username = :username", Student.class).setParameter("username", username).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Role getRoleByRoleName(String roleName) {
        return entityManager.createQuery("SELECT r FROM Role r WHERE r.name = :name", Role.class).setParameter("name", roleName).getSingleResult();
    }

}
