package hu.csk.op.service;

import hu.csk.op.dto.StudentDTO;
import hu.csk.op.entity.Role;
import hu.csk.op.entity.Student;
import hu.csk.op.facade.EntityFacade;
import hu.csk.op.facade.UserFacade;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class RegistrationService {

    @Inject
    private EntityFacade entityFacade;

    @Inject
    private UserFacade userFacade;

    private static final String STUDENT_ROLE = "STUDENT";

    public void registerStudent(StudentDTO dto) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] digest = md.digest(dto.getPassword().getBytes());
        String b64PasswordHash = Base64.getEncoder().encodeToString(digest);

        dto.setPassword(b64PasswordHash);

        Student student = new Student(dto);
        Role studentRole = userFacade.getRoleByRoleName(STUDENT_ROLE);
        student.getRoles().add(studentRole);

        entityFacade.create(student);

    }

    public boolean checkIfUserExists(StudentDTO dto) {

        Student s = userFacade.getStudentByUsername(dto.getUsername());

        return null != s;
    }
}
