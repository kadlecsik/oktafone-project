package hu.csk.op.entity;

import hu.csk.op.dto.CourseDTO;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "course")
public class Course extends Versionable implements Serializable {

    private static final long serialVersionUID = 8320221416731845241L;

    @Id
    @GeneratedValue
    private Long id;

    

    @Basic
    private String description;

    @Basic
    @Column(name = "student_limit")
    private Integer limit;

    @ManyToMany(targetEntity = Student.class,
            mappedBy = "courses",
            fetch = FetchType.EAGER)
    private List<Student> students;

    @Basic
    private String name;

    public Course() {
        //Entity - parameterless constructor
    }

    public Course(CourseDTO dto) {
        this.name = dto.getName();
        this.description = dto.getDescription();
        this.limit = dto.getLimit();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Course other = (Course) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
