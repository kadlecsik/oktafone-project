package hu.csk.op.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class CourseIsFullException extends RuntimeException {

    private static final long serialVersionUID = 4555661338547937532L;

    public CourseIsFullException() {
        super();
    }

    public CourseIsFullException(String message) {
        super(message);
    }
}
