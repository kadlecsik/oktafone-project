package hu.csk.op.dto;

import hu.csk.op.entity.Student;
import java.io.Serializable;

public class StudentDTO implements Serializable {

    private static final long serialVersionUID = 7128214614986554185L;

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    public StudentDTO() {
        //DTO - parameterless constructor
    }

    public StudentDTO(String username, String password, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public StudentDTO(Student student) {
        this(student.getUsername(), student.getPassword(), student.getFirstName(), student.getLastName());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return firstName + " " + lastName;
    }

}
