package hu.csk.op.facade;

import hu.csk.op.entity.Versionable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import javax.persistence.LockModeType;

@Stateless
public class EntityFacade {

    @PersistenceContext(unitName = "OP_PU")
    private EntityManager entityManager;

    public void flush() {
        entityManager.flush();
    }

    public <T> void create(T entity) {
        entityManager.persist(entity);
    }

    public <T> void update(T entity) {
        if (entity instanceof Versionable) {
            entityManager.lock(entity, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
        }
        entityManager.merge(entity);
        entityManager.flush();
    }

    public <T> void delete(T entity) {
        entityManager.remove(entity);
    }

    public <T> T find(Class<T> clazz, Object id) {
        return entityManager.find(clazz, id);
    }

    public <T> List<T> findAll(Class<T> entityClass) {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return entityManager.createQuery(cq).getResultList();
    }

}
