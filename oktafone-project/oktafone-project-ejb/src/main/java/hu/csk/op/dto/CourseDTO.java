package hu.csk.op.dto;

import hu.csk.op.entity.Course;
import java.util.Objects;

public class CourseDTO {

    private Long id;

    private String description;

    private Integer limit;

    private String name;

    private Integer numberOfstudents;

    public CourseDTO() {
        //DTO - parameterless constructor
    }

    public CourseDTO(String description, Integer limit, String name) {
        this.description = description;
        this.limit = limit;
        this.name = name;
    }

    public CourseDTO(Course course) {
        this(course.getDescription(), course.getLimit(), course.getName());
        this.id = course.getId();
        this.numberOfstudents = course.getStudents().size();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfstudents() {
        return numberOfstudents;
    }

    public void setNumberOfstudents(Integer numberOfstudents) {
        this.numberOfstudents = numberOfstudents;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CourseDTO other = (CourseDTO) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
