package hu.csk.op.service;

import hu.csk.op.dto.CourseDTO;
import hu.csk.op.entity.Course;
import hu.csk.op.entity.Student;
import hu.csk.op.facade.EntityFacade;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class CourseService {

    @Inject
    private EntityFacade entityFacade;

    public List<CourseDTO> getCourses() {
        List<Course> courses = entityFacade.findAll(Course.class);

        List<CourseDTO> ret = new LinkedList<>();

        courses.stream().forEach(c -> ret.add(new CourseDTO(c)));

        return ret;
    }

    public void addCourse(CourseDTO dto) {
        Course course = new Course(dto);

        entityFacade.create(course);
    }

    public void deleteCourse(Long id) {
        Course course = entityFacade.find(Course.class, id);

        List<Student> students = entityFacade.findAll(Student.class);

        for (Student s : students) {
            if (s.getCourses().contains(course)) {
                s.getCourses().remove(course);
                entityFacade.update(s);
            }
        }

        entityFacade.delete(course);
    }

}
