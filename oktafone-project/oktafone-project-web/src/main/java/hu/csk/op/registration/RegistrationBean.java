package hu.csk.op.registration;

import hu.csk.op.dto.StudentDTO;
import hu.csk.op.service.RegistrationService;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.util.Ajax;

@Named("Registration")
@ViewScoped
public class RegistrationBean implements Serializable {

    private static final long serialVersionUID = -4629549653123139802L;

    @Inject
    private RegistrationService registrationService;

    private String fistName;

    private String lastName;

    private String username;

    private String password;

    private String password2;

    private void clearFields() {
        fistName = null;
        lastName = null;
        username = null;
        password = null;
        password2 = null;
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public void registerStudent() throws NoSuchAlgorithmException {

        if (registrationService.checkIfUserExists(new StudentDTO(username, password, fistName, lastName))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "User already exists.", ""));
        } else {
            registrationService.registerStudent(new StudentDTO(username, password, fistName, lastName));
            clearFields();
            Ajax.oncomplete("closeRegDialog()");
        }
    }

}
