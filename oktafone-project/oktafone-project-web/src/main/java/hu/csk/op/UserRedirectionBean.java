package hu.csk.op;

import java.io.IOException;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

@ManagedBean
public class UserRedirectionBean {

    public void redirectUserIfLoggedIn() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest origRequest = (HttpServletRequest) context.getExternalContext().getRequest();
        String contextPath = origRequest.getContextPath();

        if (context.getExternalContext().getRemoteUser() != null) {
            try {
                context.getExternalContext().redirect(contextPath + "/app/");
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).severe(ex.getMessage());
                context.getExternalContext().setResponseStatus(404);
            }
        }
    }

}
