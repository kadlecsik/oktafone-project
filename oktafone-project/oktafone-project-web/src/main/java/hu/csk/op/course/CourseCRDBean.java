package hu.csk.op.course;

import hu.csk.op.dto.CourseDTO;
import hu.csk.op.service.CourseService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.util.Ajax;

@Named
@ViewScoped
public class CourseCRDBean implements Serializable {

    private static final long serialVersionUID = -4858167582381117244L;

    @Inject
    private CourseService courseService;

    private List<CourseDTO> courses;

    private String courseName;

    private String courseDescription;

    private Integer courseLimit;

    private void reset() {
        courseName = null;
        courseDescription = null;
        courseLimit = null;
    }

    private void update() {
        courses = courseService.getCourses();
    }

    @PostConstruct
    private void init() {
        update();
    }

    public void addCourse() {
        courseService.addCourse(new CourseDTO(courseDescription, courseLimit, courseName));
        reset();
        update();
        Ajax.updateAll();
    }

    public void deleteCourse(Long id) {
        courseService.deleteCourse(id);
        update();
        Ajax.updateAll();
    }

    public List<CourseDTO> getCourses() {
        return courses;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public Integer getCourseLimit() {
        return courseLimit;
    }

    public void setCourseLimit(Integer courseLimit) {
        this.courseLimit = courseLimit;
    }

}
