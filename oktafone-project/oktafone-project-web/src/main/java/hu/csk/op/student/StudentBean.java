package hu.csk.op.student;

import hu.csk.op.dto.CourseDTO;
import hu.csk.op.dto.StudentDTO;
import hu.csk.op.exception.CourseIsFullException;
import hu.csk.op.service.CourseService;
import hu.csk.op.service.StudentService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.util.Ajax;

@Named
@ViewScoped
public class StudentBean implements Serializable {

    @Inject
    private CourseService courseService;

    @Inject
    private StudentService studentService;

    private static final long serialVersionUID = -6574672901195134146L;

    private List<CourseDTO> courses;

    private List<CourseDTO> takenCourses;

    private StudentDTO actualStudent;

    private void update() {
        FacesContext context = FacesContext.getCurrentInstance();

        String id = context.getExternalContext().getRemoteUser();
        actualStudent = studentService.getStudentById(id);

        courses = courseService.getCourses();
        takenCourses = studentService.getCoursesForStudentByStudentId(id);

        courses.removeAll(takenCourses);
    }

    @PostConstruct
    private void init() {
        update();
    }

    public void subscribe(String studentId, Long courseId) {

       
        try {
            studentService.subscribeToCourseByIds(studentId, courseId);
        } catch (CourseIsFullException ex) {
            Ajax.oncomplete("alert('Course limit exceeded')");
        }
        update();
        Ajax.updateAll();
       
    }

    public void unsubscribe(String studentId, Long courseId) {
        studentService.unSubscribeFromCourseByIds(studentId, courseId);
        update();
        Ajax.updateAll();
    }

    public List<CourseDTO> getTakenCourses() {
        return takenCourses;
    }

    public List<CourseDTO> getCourses() {
        return courses;
    }

    public StudentDTO getActualStudent() {
        return actualStudent;
    }

}
